use cubers::{
    cube::{self, Cube2, Cube3},
    Puzzle,
};

fn main() -> Result<(), String> {
    let mut c = Cube2::default();
    c.print()?;
    println!("{:?}", c.solved());

    let moves = cube::random_moves2(3);
    println!();
    for m in &moves {
        print!("{} ", m);
    }
    println!();
    c.apply_moves(&moves)?;
    c.print()?;
    println!("{:?}", c.solved());
    println!();
    c.apply_moves(&moves)?;
    c.print()?;
    println!("{:?}", c.solved());

    let mut c = Cube3::default();
    c.print()?;
    println!("{:?}", c.solved());

    let moves = cube::random_moves3(3);
    println!();
    for m in &moves {
        print!("{} ", m);
    }
    println!();
    c.apply_moves(&moves)?;
    c.print()?;
    println!("{:?}", c.solved());
    println!();
    c.apply_moves(&moves)?;
    c.print()?;
    println!("{:?}", c.solved());
    Ok(())
}
