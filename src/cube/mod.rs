use crate::Move;
use alloc::{
    format,
    str::FromStr,
    string::{String, ToString},
    vec::Vec,
};
#[cfg(feature = "color")]
use colored::*;
mod cube2;
mod cube3;
use alloc::fmt::{Display, Formatter};
pub use cube2::*;
pub use cube3::*;

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub enum CubeFace {
    U,
    D,
    F,
    B,
    L,
    R,
}

impl Display for CubeFace {
    fn fmt(&self, f: &mut Formatter) -> alloc::fmt::Result {
        match &self {
            CubeFace::U => write!(f, "U"),
            CubeFace::D => write!(f, "D"),
            CubeFace::F => write!(f, "F"),
            CubeFace::B => write!(f, "B"),
            CubeFace::L => write!(f, "L"),
            CubeFace::R => write!(f, "R"),
        }
    }
}

impl FromStr for CubeFace {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use CubeFace::*;
        if s.len() == 0 {
            return Err("empty string received".to_string());
        }
        match &s[0..1] {
            "U" => Ok(U),
            "D" => Ok(D),
            "L" => Ok(L),
            "R" => Ok(R),
            "F" => Ok(F),
            "B" => Ok(B),
            x => Err(format!("{} is no valid face", x)),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub enum MoveType {
    CW,
    CCW,
    Double,
}

impl Display for MoveType {
    fn fmt(&self, f: &mut Formatter) -> alloc::fmt::Result {
        match &self {
            MoveType::CW => write!(f, ""),
            MoveType::CCW => write!(f, "'"),
            MoveType::Double => write!(f, "2"),
        }
    }
}

#[cfg(feature = "color")]
fn face_to_color(f: &CubeFace) -> ColoredString {
    use self::CubeFace::*;
    match f {
        U => "■".white(),
        D => "■".yellow(),
        L => "■".magenta(),
        R => "■".red(),
        F => "■".green(),
        B => "■".blue(),
    }
}

pub fn moves<M: Move<F> + FromStr + alloc::fmt::Debug, F>(s: &str) -> Result<Vec<M>, M::Err>
where
    M::Err: alloc::fmt::Debug,
{
    if s.len() == 0 {
        Ok(Vec::new())
    } else {
        s.split(" ").map(|x| x.parse()).collect()
    }
}
