#![no_std]
extern crate alloc;

use alloc::vec::Vec;

pub mod cube;
pub mod pyraminx;

pub trait Puzzle {
    type Face;
    type Position;
    type Move: Move<Self::Position>;
    type Error;

    fn permute(&mut self, a: &Self::Position, b: &Self::Position) -> Result<(), Self::Error>;
    fn solved(&self) -> bool;
    fn face(&self, p: &Self::Position) -> Result<&Self::Face, Self::Error>;

    fn apply_move(&mut self, m: &Self::Move) -> Result<(), Self::Error> {
        for (a, b) in m.permutations() {
            self.permute(&a, &b)?;
        }
        Ok(())
    }

    fn apply_moves(&mut self, moves: &[Self::Move]) -> Result<(), Self::Error> {
        for m in moves {
            self.apply_move(m)?;
        }
        Ok(())
    }
}

pub trait Move<P> {
    fn permutations(&self) -> Vec<(P, P)>;
}

pub trait CycleMove<P> {
    fn cycles(&self) -> Vec<Vec<P>>;
}

impl<P, C> Move<P> for C
where
    C: CycleMove<P>,
    P: Clone + alloc::fmt::Debug,
{
    fn permutations(&self) -> Vec<(P, P)> {
        self.cycles()
            .iter()
            .map(|c| {
                let mut a = c.clone();
                a.reverse();
                let n = a.len();
                let b = [&a[1..n], &a[0..1]].concat();
                a.iter()
                    .cloned()
                    .zip(b.iter().cloned())
                    .skip(1)
                    .collect::<Vec<_>>()
            })
            .flatten()
            .collect()
    }
}
